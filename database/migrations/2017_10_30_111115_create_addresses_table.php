<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cep');
            $table->string('street');
            $table->integer('number');
            $table->string('complement');
            $table->integer('city_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('addresses', function($table) {
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('user_id')->references('id')->on('users');
        });

        DB::table('addresses')->insert([
            [
                'id' => 1,
                'cep' => '88015-410',
                'street' => 'Rua Frei Evaristo',
                'number' => 86,
                'complement' => 'Apto 302',
                'city_id' => 1,
                'user_id' => 1
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
