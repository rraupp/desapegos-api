<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->char('id', 2);
            $table->string('state');
            $table->char('country_id', 2);
            $table->timestamps();
        });

        Schema::table('states', function($table) {
            $table->primary('id');
            $table->foreign('country_id')->references('id')->on('countries');
        });

        DB::table('states')->insert([
            [
                'id' => 'SC',
                'state' => 'Santa Catarina',
                'country_id' => 'BR'
            ],
            [
                'id' => 'SP',
                'state' => 'Sao Paulo',
                'country_id' => 'BR'
            ],
            [
                'id' => 'PR',
                'state' => 'Paraná',
                'country_id' => 'BR'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
