<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->decimal('rental_price')->unsigned();
            $table->boolean('for_sale');
            $table->decimal('sale_price')->unsigned()->nullable();
            $table->boolean('available')->default(1);
            $table->integer('category_id')->unsigned()->index();
            $table->integer('size_id')->unsigned()->index()->nullable();
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('products', function($table) {
            $table->foreign('size_id')->references('id')->on('sizes');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('products');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
