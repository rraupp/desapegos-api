<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('size_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->timestamps();
        });

        DB::table('size_types')->insert([
            [
                'id' => 1,
                'type' => 'NO_SIZE'
            ],
            [
                'id' => 2,
                'type' => 'ALPHABETIC'
            ],
            [
                'id' => 3,
                'type' => 'NUMERIC_LEGS'
            ],
            [
                'id' => 4,
                'type' => 'NUMERIC_SHOES'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('size_types');
    }
}
