<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'Rafael Raupp',
                'email' => 'raupperino@gmail.com',
                'password' => '$2y$10$2zgblhsITLhH.Sw/NXO3melQWr3Ljx/hgLC6l5N3uD5LTBhvELBiu',
                'created_at' => '2017-10-30 16:41:32',
                'updated_at' => '2017-10-30 16:41:32'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
