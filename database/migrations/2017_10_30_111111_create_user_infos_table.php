<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            //$table->increments('id');
            $table->string('cpf');
            $table->date('birth_date');
            $table->string('profile_picture');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('user_infos', function($table) {
            $table->primary('cpf');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        DB::table('user_infos')->insert([
            [
                'cpf' => '075.262.359-14',
                'birth_date' => '1996/07/10',
                'profile_picture' => 'no_img',
                'user_id' => 1
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
