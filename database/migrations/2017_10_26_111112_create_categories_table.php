<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('category');
            $table->integer('size_type')->unsigned();
            $table->timestamps();
        });

        Schema::table('categories', function($table) {
            $table->foreign('size_type')->references('id')->on('size_types');
        });

        DB::table('categories')->insert([
            [
                'id' => 1,
                'category' => 'Camisas',
                'size_type' => 1
            ],
            [
                'id' => 2,
                'category' => 'Calças',
                'size_type' => 1

            ],
            [
                'id' => 3,
                'category' => 'Casacos',
                'size_type' => 1
            ],
            [
                'id' => 4,
                'category' => 'Vestidos',
                'size_type' => 1
            ],
            [
                'id' => 5,
                'category' => 'Shorts',
                'size_type' => 1
            ],
            [
                'id' => 6,
                'category' => 'Sapatos',
                'size_type' => 1
            ],
            [
                'id' => 7,
                'category' => 'Acessórios',
                'size_type' => 1
            ],
            [
                'id' => 8,
                'category' => 'Outros',
                'size_type' => 1
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
