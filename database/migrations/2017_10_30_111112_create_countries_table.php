<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->char('id', 2);
            $table->string('country');
            $table->timestamps();
        });

        Schema::table('countries', function($table) {
            $table->primary('id');
        });

        DB::table('countries')->insert([
            [
                'id' => 'BR',
                'country' => 'Brazil'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
