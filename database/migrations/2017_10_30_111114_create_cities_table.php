<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city');
            $table->char('state_id', 2);
            $table->timestamps();
        });

        Schema::table('cities', function($table) {
            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::table('cities')->insert([
            [
                'id' => 1,
                'city' => 'Florianopolis',
                'state_id' => 'SC'
            ],
            [
                'id' => 2,
                'city' => 'São José',
                'state_id' => 'SC'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
