<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('size');
            $table->integer('size_type')->unsigned();
            $table->timestamps();
        });

        Schema::table('sizes', function($table) {
            $table->foreign('size_type')->references('id')->on('size_types');
        });

        DB::table('sizes')->insert([
            [
                'id' => 1,
                'size' => 'UNDEFINED',
                'size_type' => 1
            ],
            [
                'id' => 2,
                'size' => 'XS',
                'size_type' => 2
            ],
            [
                'id' => 3,
                'size' => 'S',
                'size_type' => 2
            ],
            [
                'id' => 4,
                'size' => 'M',
                'size_type' => 2
            ],
            [
                'id' => 5,
                'size' => 'L',
                'size_type' => 2
            ],
            [
                'id' => 6,
                'size' => 'XL',
                'size_type' => 2
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sizes');
    }
}
