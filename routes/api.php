<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/register', 'RegisterController@register');


Route::group(['prefix' => 'products'], function() {
    Route::get('/', 'ProductController@index');
    Route::get('/{product}', 'ProductController@show');
    Route::post('/', 'ProductController@store')->middleware('auth:api');
    Route::patch('/{product}', 'ProductController@update')->middleware('auth:api');
    Route::delete('/{product}', 'ProductController@destroy')->middleware('auth:api');
});