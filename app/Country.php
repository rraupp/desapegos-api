<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $table = 'countries';

    public $incrementing = false;

    public function states()
    {
        return $this->hasMany(State::class);
    }
}
