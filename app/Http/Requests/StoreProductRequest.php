<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'images' => 'required',
            'cover_index' => 'required',
            'title' => 'required|min:1|max:255',
            'description' => '',
            'category' => 'required',
            'size' => '',
            'tags' => 'required',
            'rental_price' => 'required',
            'for_sale' => 'required',
            'sale_price' => ''
        ];
    }
}
