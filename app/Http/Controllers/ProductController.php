<?php

namespace App\Http\Controllers;

use App\Product;
use App\Image;
use App\Tag;
use App\Category;
use App\Size;
use App\Http\Requests\StoreProductRequest;
use App\Transformers\ProductTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::LatestFirst()->paginate(15);
        $productsCollection = $products->getCollection();

        return fractal()
            ->collection($productsCollection)
            ->parseIncludes(
                [
                    'size',
                    'user.address.city'
                ]
            )
            ->paginateWith(new IlluminatePaginatorAdapter($products))
            ->transformWith(new ProductTransformer)
            ->toArray();
    }

    public function show(Product $product)
    {
        return fractal()
            ->item($product)
            ->parseIncludes([
                'images',
                'tags',
                'category',
                'size',
                //'user',
                //'user.address',
                'user.address.city'])
            ->transformWith(new ProductTransformer)
            ->toArray();
    }

    public function store(StoreProductRequest $request)
    {
        // Creates the products
        $product = new Product;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->rental_price = $request->rental_price;
        $product->for_sale = $request->for_sale;
        $product->sale_price = $request->sale_price;

        // Checks if category exists
        $category = Category::find($request->category);
        if($category == null)
        {
            $error_message = 'That category does not exist.';
            return json_encode($error_message);
            die();
        }
        $product->category()->associate($category);

        // TODO implement size
        if($request->size != null)
        {
            $size = Size::find($request->size);
            if($size == null)
            {
                $error_message = 'That size does not exist.';
                return json_encode($error_message);
                die();
            }
            $product->size()->associate($size);
        }
        else
        {
            $product->size()->associate(Size::find(1));
        }


        // Associates with user and saves
        $product->user()->associate($request->user());
        $product->save();

        // Persists and attaches tags
        foreach ($request->tags as $request_tag)
        {
            $tag = null;
            try
            {
                $tag = Tag::where('tag', '=', $request_tag)->firstOrFail();
            }
            catch (ModelNotFoundException $ex)
            {
                $tag = new Tag;
                $tag->tag = $request_tag;
                $tag->save();
            }
            $product->tags()->attach($tag);
        }

        // Persists and associates images
        $images = [];
        foreach ($request->images as $index => $request_image)
        {
            $image = new Image;
            $image->file_name = $request_image;
            $image->cover = $index == $request->cover_index;
            $image->product()->associate($product);
            $image->save();
            $images[] = $image;
        }

        // Returns transformed object
        return fractal()
            ->item($product)
            ->parseIncludes(['tags', 'category', 'size', 'user'])
            ->transformWith(new ProductTransformer)
            ->toArray();
    }

    public function destroy(Product $product)
    {
        $this->authorize('destroy', $product);
    }

}
