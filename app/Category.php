<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public function sizeType()
    {
        return $this->hasOne(SizeType::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    protected $table = 'categories';
}
