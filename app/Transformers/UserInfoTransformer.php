<?php

namespace App\Transformers;

use App\UserInfo;
class UserInfoTransformer extends \League\Fractal\TransformerAbstract
{

    public function transform(UserInfo $info)
    {
        return [
            'cpf' => $info->cpf,
            'birth_date' => $info->birth_date,
            'profile_picture' => $info->profile_picture
        ];
    }

}