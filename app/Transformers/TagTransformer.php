<?php

namespace App\Transformers;

use App\Tag;

class TagTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(Tag $tag)
    {
        return [
            'id' => $tag->id,
            'tag' => $tag->tag
        ];
    }
}