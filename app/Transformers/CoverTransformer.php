<?php

namespace App\Transformers;

use App\Image;

class CoverTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(Image $cover)
    {
        return [
            'id' => $cover->id,
            'file_name' => $cover->file_name
        ];
    }
}