<?php

namespace App\Transformers;

use App\Size;

class SizeTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(Size $size)
    {
        return [
            'id' => $size->id,
            'size' => $size->size
        ];
    }
}