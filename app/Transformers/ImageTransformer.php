<?php

namespace App\Transformers;

use App\Image;

class ImageTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(Image $image)
    {
        return [
            'id' => $image->id,
            'file_name' => $image->file_name
            //'cover' => $image->cover
        ];
    }
}