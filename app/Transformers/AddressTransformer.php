<?php

namespace App\Transformers;

use App\Address;
class AddressTransformer extends \League\Fractal\TransformerAbstract
{

    protected $availableIncludes = ['city'];

    public function transform(Address $address)
    {
        return [
            //'id' => $address->id,
            'cep' => $address->cep,
            //'street' => $address->street,
            //'number' => $address->number,
            //'complement' => $address->complement
        ];
    }

    public function includeCity(Address $address)
    {
        return $this->item($address->city, new CityTransformer());
    }

}