<?php

namespace App\Transformers;

use App\User;

class UserTransformer extends \League\Fractal\TransformerAbstract
{

    protected $availableIncludes = ['info', 'address'];

    public function transform(User $user)
    {
        return [
            'name' => $user->name,
            'email' => $user->email,
            'avatar' => $user->avatar()
        ];
    }

    public function includeInfo(User $user)
    {
        return $this->item($user->info, new UserInfoTransformer());
    }

    public function includeAddress(User $user)
    {
        return $this->item($user->address, new AddressTransformer());
    }
}