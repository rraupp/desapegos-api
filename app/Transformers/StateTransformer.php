<?php

namespace App\Transformers;

use App\State;
class StateTransformer extends \League\Fractal\TransformerAbstract
{

//    protected $availableIncludes = ['country'];

    public function transform(State $state)
    {
        return [
            'id' => $state->id,
            'state' => $state->state,
            'country_id' => $state->country_id
        ];
    }

//    public function includeCountry(State $state)
//    {
//        return $this->item($state->country, new StateTransformer());
//    }

}