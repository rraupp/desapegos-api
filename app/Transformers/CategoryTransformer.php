<?php

namespace App\Transformers;

use App\Category;

class CategoryTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(Category $category)
    {
        return [
            'id' => $category->id,
            'category' => $category->category
        ];
    }
}