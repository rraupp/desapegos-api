<?php

namespace App\Transformers;

use App\Product;

class ProductTransformer extends \League\Fractal\TransformerAbstract
{

    protected $availableIncludes = ['cover', 'images', 'tags', 'category', 'size', 'user', 'likeCount'];

//    public function __construct($showAvailable)
//    {
//        $this->showAvailable = $showAvailable;
//    }

    public function transform(Product $product)
    {
//        $p = [
//            'id'            => $product->id,
//            'title'         => $product->title,
//            'description'   => $product->description,
//            'rental_price'  => $product->rental_price,
//            'for_sale'      => $product->for_sale,
//            'sale_price'    => $product->sale_price,
//            'available'     => $product->available
//        ];
        return [
            'id'            => $product->id,
            'title'         => $product->title,
            'description'   => $product->description,
            'rental_price'  => $product->rental_price,
            'for_sale'      => $product->for_sale,
            'sale_price'    => $product->sale_price,
            'available'     => $product->available
        ];
    }

    public function includeImages(Product $product)
    {
        return $this->collection($product->images, new ImageTransformer);
    }

    public function includeCover(Product $product)
    {
        return $this->item($product->cover, new CoverTransformer);
    }

    public function includeAvailable(Product $product)
    {
        return $this->item($product->available, function()
        {
            dd($product->available);
        });
    }

    public function includeTags(Product $product)
    {
        return $this->collection($product->tags()->get(), new TagTransformer());
    }

    public function includeCategory(Product $product)
    {
        return $this->item($product->category, new CategoryTransformer);
    }

    public function includeSize(Product $product)
    {
        return $this->item($product->size, new SizeTransformer);
    }

    public function includeUser(Product $product)
    {
        return $this->item($product->user, new UserTransformer);
    }

    public function includeLikeCount()
    {

    }

}