<?php

namespace App\Transformers;

use App\City;
class CityTransformer extends \League\Fractal\TransformerAbstract
{

    protected $availableIncludes = ['state'];

    public function transform(City $city)
    {
        return [
            'id' => $city->id,
            'city' => $city->city,
            'state_id' => $city->state_id
        ];
    }

    public function includeState(City $city)
    {
        return $this->item($city->state, new StateTransformer);
    }

}