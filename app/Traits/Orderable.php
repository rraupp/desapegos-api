<?php

namespace App\Traits;

trait Orderable
{
    public function scopeLatestFirst($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeOldestFirst($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    public function scopeLowestRental($query)
    {
        return $query->orderBy('rental_price', 'asc');
    }

    public function scopeHighestRental($query)
    {
        return $query->orderBy('rental_price', 'desc');
    }
}