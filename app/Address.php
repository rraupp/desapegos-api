<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    protected $table = 'addresses';

    protected $hidden = [
        'street',
        'number',
        'complement'
    ];

    public function user()
    {
        return $this->belongsTo(UserInfo::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
