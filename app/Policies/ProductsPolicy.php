<?php
namespace App\Policies;

use App\User;
use App\Product;
use Illuminate\Auth\Access\HandlesAuthorization;
class ProductsPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Product $product)
    {
        return $user->ownsProduct($product);
    }

    public function destroy(User $user, Product $product)
    {
        return $user->ownsProduct($product);
    }
}